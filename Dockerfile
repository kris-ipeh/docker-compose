FROM docker:18.09.6

# install dependencies for docker-compose
# dependencies according to 
# - https://docs.docker.com/compose/install/
# - https://wiki.alpinelinux.org/wiki/Docker#Docker_Compose
RUN apk add --no-cache \
    python-dev=2.7.16-r1 \
    py-pip=18.1-r0 \
    libffi-dev=3.2.1-r6 \
    openssl-dev=1.1.1b-r1 \
    gcc=8.3.0-r0 \
    libc-dev=0.7.1-r0 \
    make=4.2.1-r2

# install docker-compose
RUN pip install docker-compose==1.24.0

# simple check, is docker-compose executable? 
RUN docker-compose -v