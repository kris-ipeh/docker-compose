# docker-compose

This repository provides a Dockerfile that could be used throughout our applications CI builds that rely on `docker-compose` as an orchestrator for multiple containers. 

Contrary to qmarketingoss/dind-docker-compose, this image will not start a docker daemon on its own. It'll be used as a docker client in CI builds.  

## Problem

The `docker:*` images used in gitlab CI's documentation do not contain `docker-compose` out of the box. Because of that we're required to pull that requirement into our build-time images each and every time. That takes time and eats up CPU and bandwidth resources that could be used in a more useful manner. 

To overcome that problem for our projects that rely on a `docker-compose.yml` as the orchestrator of multiple interacting containers, we needed to build a base image we can rely on in our `.gitlab-ci.yml` files. 

## Configuration

I'd like to avoid necessary configuration within that super simple repository as much as possible. So, I've just enabled Gitlab's AutoDevOps feature for it. The `Dockerfile` got recognized out of the box, but the pipeline also did setup jobs for `code-quality` and `tests` (which was failing, as the repository didn't, nor will it in the future, contain any tests).

Fortunately, Gitlab's AutoDevOps feature is configurable using CI/CD variables. To disable the `code-quality` and `tests` stages for AutoDevOps, add the following variables to your CI configuration: 

- `CODE_QUALITY_DISABLED: true` (See AutoDevOps's [Code-Quality.gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab-ce/blob/acb55198b4a05a0b4ac2662bf68cfeb3d744ca01/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml#L27))
- `TEST_DISABLED: true` (See AutoDevOps's [Test.gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab-ce/blob/acb55198b4a05a0b4ac2662bf68cfeb3d744ca01/lib/gitlab/ci/templates/Jobs/Test.gitlab-ci.yml#L23))

## Build

```sh
docker build -t docker-compose .
```

## Publish to `hub.docker.com`

The built docker image will be frequently used throughout our CI builds. 

As we at Q:MARKETING host our projects on [Gitlab.com](https://gitlab.com) and also make use of Gitlab's Continuous Integration feature, we heavily rely on shared and also on dedicated Gitlab Runners. For security reasons, most Gitlab Runners aren't configured to trust arbitrary docker image registries. 

> By default, the executor will only pull images from Docker Hub, but this can be configured in the gitlab-runner/config.toml by setting the Docker pull policy to allow using local images.

(Source: https://docs.gitlab.com/ee/ci/docker/using_docker_images.html)

In order to make it possible to use the built image in conjunction with shared and also our own dedicated Gitlab Runner, it is necessary to release the built image manually to [`hub.docker.com`](https://hub.docker.com/qmarketing/docker-compose).

Assuming, you've already built the image locally (possibly with name `docker-compose`), you can tag the image with another name:

### Publish version 'latest'

**Tag the built image:**

```sh
docker tag docker-compose:latest qmarketing/docker-compose:latest 
``` 

**Publish the image to Docker Hub:** 

```sh
docker push qmarketing/docker-compose:latest
```

### Publish a commit based version

We aren't that good providing semantic versioning. Instead, we often use a short reference to the origin's repository commit SHA.

**Tag the built image:**

```sh
docker tag qmarketing/docker-compose:latest qmarketing/docker-compose:$(git rev-parse --short HEAD)
```

**Publish the image to Docker Hub:**

```sh
docker push qmarketing/docker-compose:$(git rev-parse --short HEAD)
```